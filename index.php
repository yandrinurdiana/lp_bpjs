<?php
$cs = [
	['name' => 'Sali', 'mobile' => '6281390769373', 'email' => ''],
];
$keys = array_keys($cs);
$random = $keys[array_rand($keys,1)];
$name = $cs[$random]['name'];
$mobile = $cs[$random]['mobile'];
$email = $cs[$random]['email'];
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
	<title> Bakpia Juwara Satoe - Bakpia Crispy Gula Tarik Oleh oleh Khas Jogja</title>
	<meta name="keywords" content="bakpia pathok, bakpia kacang hijau, bakpia jogja, kue kacang, oleh oleh jogja, oleh oleh bakpia, bakpia juwara satoe, bakpia gula tarik, bakpia premium, bakpiaku, roll nastar, nastar" />
	<meta name="description" content="Oleh-oleh bakpia crispy pertama di jogja rasa gula tarik lezatnya bikin lidahmu meleleh" />
	<link rel="shortcut icon" href="https://bakpiajuwarasatoe.com/images/favicon/img_nuUL4om.png">
	<!-- ===================== MASTER CSS ===================== -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<!-- ICONS FONT PLUGINS -->
	<link rel="stylesheet" type="text/css" href="https://bakpiajuwarasatoe.com/plugins/font-awesome/css/font-awesome.min.css" />
	<!-- ===================== SITE CSS ===================== -->
	<link rel="stylesheet" type="text/css" href="https://bakpiajuwarasatoe.com/plugins/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="assets/css/theme.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css" />
	<!-- ===================== RESPONSIVE CSS ===================== -->
	<link rel="stylesheet" type="text/css" href="https://bakpiajuwarasatoe.com/css/responsive.css?ver=1.0">
	<!-- LIBRARY CORE -->
	<script src="https://bakpiajuwarasatoe.com/js/jquery.min.js"></script>
	<script src="https://bakpiajuwarasatoe.com/js/bootstrap.min.js"></script>
	<!-- Alert -->
	<script src="https://cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
	<!-- SITE JS -->
	<script src="https://bakpiajuwarasatoe.com/js/app.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131395595-1"></script>

	<link rel="stylesheet" href="https://d1zviajkun9gxg.cloudfront.net/content/vendor/fontawesome/css/fontawesome-5.min.css" />
	<link rel="stylesheet" href="assets/css/whatsapp_style.css">


	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<style type="text/css">
		body{
			font-family: 'Lato', sans-serif;
			font-size:19px;
		}

		.title_bpjs{
			padding-top: 50px;
			font-family: 'Lato', sans-serif !important;
			font-size: 29px;

		}
		#inner {
			display: table;
			margin: 0 auto;
		}

		#outer {
			width: 100%
		}
		#outer_text {
			width: 100%
		}
		.checklist-ul {
			list-style-type: none;
			padding: 0;}
			li {
				padding: 0 0 0 25px;
				position: relative;
				margin-bottom: 5px;
				text-align: left;
			}
			li:before {
				position: absolute;
				left: 10px;
				content: "\2713";
				color: #666;

			}
		</style>

	</head>

	<body class="loading">
		<div class="kotak-loading">
			<div class="box">
				<img src="https://bakpiajuwarasatoe.com/images/background/loading.gif">
			</div>
		</div>
		<!-- Navbar Header -->
		<nav class="navbar" role="navigation">
			<div class="container-fluid">
				<div class="visible-sm visible-xs">

					<a href="#" class="logo-mobile">
						<img src="assets/img/bakpia_logo.png" alt="logo">			</a>
					</div>

				</div>
			</nav>

			<section id="news" style="padding:0">
				<div class="news-bg"><img src="https://bakpiajuwarasatoe.com/images/background/bg-png.png" alt=""></div>
			</section>
			<section id="karakter">
				<div class="karakter-bg hidden-sm hidden-xs hidden-sm" style="transform:inherit"><img src="https://bakpiajuwarasatoe.com/images/background/gula-tarik.jpg" alt="gula tarik"></div>
				<!--div class="karakter-bg-bottom hidden-sm hidden-xs" style="transform: rotateY(180deg);"><img src="https://bakpiajuwarasatoe.com/images/background/gula_tarik.jpg" alt="gula tarik"></div-->
				<div class="container">
					<div class="title_bpjs" >Cari oleh-oleh Khas Jogja yang Murah dan Enak?</div>
					<br>
					<p>Kebanyakan orang luar kota Jogja ingin menikmati Bakpia, tapi
						mudah basi,  harga mahal, varian rasa tidak banyak.
					</p>
					<br>
					<p>Jauh dari Jogja dan rindu Bakpia Jogja yang memiliki cita rasa yang khas, enak, dan murah ?<br>
						Ingin menikmati Bakpia, tapi ketika sampai rumah, sudah basi? 
					</p>
					<br>
					<div id="outer">
						<div id="inner"><img src="assets/img/gambar1.png" style="width: 90%"></div></div>
						<br>
						<p><span style="color: black;font-weight:bold; font-size:22px;">Jangan khawatir, <br>Bakpia Juwara Satoe menggunakan bahan berkualitas yang tahan lama.</span><br><br> 
							Diolah dengan mesin berteknologi tinggi yang canggih dan aman bisa tahan sampai 6 bulan, dan pastinya bebas dari bahan pengawet
						</p>
						<div id="outer">
							<div id="inner"><img src="assets/img/gambar2.png" style="width: 95%"></div></div>
							<br>
							<p>Varian rasa Bakpia Juwara Satoe <br>tidak hanya tersedia rasa kacang hijau saja, tapi ada banyak unggulan rasa yaitu Gula tarik, rasa sambal, Pandan, Roll Nastar, dan Nastar Jumbo yang menjadi salah satu favorit para wisatawan yang berkunjung ke Yogyakarta karena memiliki cita rasa yang khas dan istimewa seperti Yogyakarta.</p>
							<div id="outer">
								<div id="inner"><img src="assets/img/gambar3.png" style="width: 90%"></div></div>
								<br>
								<p><span style="color: black;font-weight:bold; font-size:20px;">Spesial Paket Bakpia Juwara Satoe</span></p>
								<div id="outer"><div id="inner"><img src="assets/img/gambar4.png" style="width: 90%"></div></div><br>
								<div id="outer"><div id="inner"><img src="assets/img/gambar5.png" style="width: 90%"></div></div><br>
								<div id="outer"><div id="inner"><img src="assets/img/gambar6.png" style="width: 90%"></div></div><br>
								<div id="outer"><div id="inner"><img src="assets/img/gambar7.png" style="width: 90%"></div></div><br>
								<p>Sejak gigitan pertama, crispy nya sudah terasa gurih dan lezat.<br> Memiliki rasa yang berbeda dengan bakpia pada umumnya. <br>Saking nikmatnya, akan membuat terasa sedang berada di Jogja.<br><br>
									Bakpia Juwara Satoe menjadi oleh-oleh khas daerah Yogjakarta yang diburu banyak orang, tidak hanya dari masyarakat lokal, namun juga luar kota bahkan mancanegara. <br><span style="color: black;font-weight:bold; font-size:20px;">Karena Juwaranya Bakpia, ya Bakpia Juwara Satoe!</span> 
								</p>
								<div id="outer"><div id="inner"><img src="assets/img/gambar8.png" style="width: 90%"></div></div><br>
								<div id="outer"><div id="inner"><img src="assets/img/gambar9.png" style="width: 90%"></div></div><br>
								<p><span style="color: black;font-weight:bold; font-size:20px;">Perbedaan Bakpia Juwara Satoe dibandingkan Bakpia lain :</span><br>
									<div id="outer_text"><div id="inner">
										<ul class="checklist-ul">
											<li>Varian rasa banyak</li>
											<li>Crispy dan lezat</li>
											<li>Harga Terjangkau</li>
											<li>Bebas Bahan Pengawet</li>
											<li>Higienis</li>
											<li>Tahan Lama</li>
											<li>Menggunakan Tekhnologi <br>tinggi & Modern</li>
											<li>Halal bersertifikat MUI</li>
										</ul></div></p>
										<p>Pabrik Bakpia Juwara Satoe <br>sangat besar berada di Berbah, Sleman memiliki teknologi yang sangat canggih yang menghasilkan bahan berkualitas.</p>

										<div id="outer"><div id="inner"><img src="assets/img/pabrik_bakpia1.png" style="width: 90%"></div></div><br>
										<p>Ingin menikmati gurihnya Crispy dari Bakpia Juwara Satoe yang memiliki cita rasa yang khas dari Yogyakarta?<br><br>  
											Pesan Bakpia Juwara Satoe dengan Bantuan CS kami.
											<br>Ada team CS kami yang siap membantu anda kapanpun.
										</p>
										<p><span style="color: black;font-weight:bold; font-size:18px;">ORDER BAKPIA JUWARA SATOE 
											SEKARANG JUGA !!!
										</span></p>
										<br>
										<p><span href="#" class=" wa_btn btn btn-primary btn-lg active" role="button" aria-pressed="true" style="background-color: #f8a256;    border-color: transparent;">ORDER SEKARANG</a></p>
										</div>

										<div class="whatsapp-box">
											<div class="heading">
												<button class="btn-close" type="button"><i class="fa fa-angle-left"></i></button>
												<div class="avatar"><img src="assets/img/bakpia_wa.jpg" alt=""></div>
												<div class="name"><?php echo $name;?></div>
											</div>
											<div class="chatbox">
												<div class="welcome-text" style="text-align: left;">
													Halo, Para Juwara! Saya <?php echo $name; ?> dari Bakpia Juwara Satoe. Silahkan chat saya untuk pesan.
													<div class="notice">
														<span class="time"></span>
														<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="594.149px" height="594.149px" viewBox="0 0 594.149 594.149" style="enable-background:new 0 0 594.149 594.149;" xml:space="preserve">
															<g>
																<g id="done-all">
																	<path d="M448.8,161.925l-35.7-35.7l-160.65,160.65l35.7,35.7L448.8,161.925z M555.899,126.225l-267.75,270.3l-107.1-107.1l-35.7,35.7l142.8,142.8l306-306L555.899,126.225z M0,325.125l142.8,142.8l35.7-35.7l-142.8-142.8L0,325.125z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g> 
																	</g>
																</svg>
															</div>
														</div>
														<br>

													</div>
													<form class="whatsapp-form" action="redirect.php?urls=<?php echo $urls ?>" data-phone="<?php echo $mobile; ?>">
														<input name="wa_message" type="text" class="form-control" placeholder="Reply.." value="Halo, saya mau pesan Bakpia Juwara Satoe dong!" autocomplete="off">
														<button class="btn btn-send" type="submit">
															<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="535.5px" height="535.5px" viewBox="0 0 535.5 535.5" style="enable-background:new 0 0 535.5 535.5;" xml:space="preserve"><g><g id="send"><polygon points="0,497.25 535.5,267.75 0,38.25 0,216.75 382.5,267.75 0,318.75"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
															</svg>
														</button>
													</form>
												</div>
											</section>

											<section id="karakter">


											</section>

											<!-- Footer -->


										</body>
										<!-- JS Lib -->
										<script type="text/javascript" src="https://bakpiajuwarasatoe.com/plugins/slick/slick.min.js"></script>
										<script type="text/javascript">
											$(function () {
												$('#slider').slick({
													infinite: true,
													slidesToShow: 1,
													slidesToScroll: 1,
													adaptiveHeight: true,
													autoplay: true,
													autoplaySpeed: 5000,
													dots: true,
													arrows: false,
													pauseOnHover: false
												});
												$('.carousel').carousel({
													interval: 5000,
													cycle: true,
												})
											});
										</script>

										<script>
											$('.row-karakter').slick({
												infinite: true,
												slidesToShow: 3,
												slidesToScroll: 1,
												adaptiveHeight: true,
												autoplay: false,
												autoplaySpeed: 5000,
												dots: false,
												arrows: true,
												pauseOnHover: false,
												prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="fa fa-angle-left"></i></button>',
												nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-angle-right"></i></button>',
											});
										</script>

										<script type="text/javascript" src="https://bakpiajuwarasatoe.com/plugins/scroll-reveal/scrollReveal.min.js"></script>
										<script type="text/javascript">
											window.sr = new scrollReveal();
										</script>

										<script type="text/javascript">
											$(function () {
												if ($(window).width() > 768) {
													$(window).on('scroll', function () {
														var top = $(window).scrollTop();
														var slideMove = (top * 50) / 100;
														$('#slider').css({
															'transform': 'translateY(-' + slideMove + 'px)'
														});
													});
												}
											});
										</script>
										<script type="text/javascript">
											$(function () {
												$('#bakpia-group').on('click', function () {
													if ($('#outlet').hasClass('active')) {
														$('#outlet').removeClass('active');
														console.log('active');
													} else {
														$('#outlet').addClass('active');
														console.log('non');
													}
												});
											});
										</script>

										<script type="text/javascript">
											$(function(){

												$('.wa_btn').on('click',function(){
													if($('.whatsapp-box').hasClass('active')){
														$('.whatsapp-box').removeClass('active');
													} else {
														$('.whatsapp-box').addClass('active');
														greeting();

													}
												});
												function greeting(){
													var time = new Date();
													var timeText = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
													$('.whatsapp-box .time').text(timeText);
													setTimeout(function() { $('input[name="wa_message"]').focus() }, 1000);

													var curHr = time.getHours()

													if (curHr < 10) {
														var greeting = 'pagi';
													} else if (curHr < 15) {
														var greeting = 'siang';
													} else if (curHr < 18) {
														var greeting = 'sore';
													} else {
														var greeting = 'malam';
													}
													$('.whatsapp-box .greeting').text(greeting);
												}
												$('.whatsapp-box .btn-close').on('click',function(){
													$('.whatsapp-box').removeClass('active');
												});

												$('.whatsapp-form').on('submit',function(){
													var url;
													var phone = $(this).data('phone');
													var message = $('.whatsapp-form input[name=wa_message]').val();
													if($(window).width() > 768){
														url = "https://web.whatsapp.com/send?phone="+phone+"&text="+message+"";
													} else {
														url = "https://wa.me/"+phone+"?text="+message+"";
													}
													window.open("redirect.php?urls="+url+"");
													return false;
												});
												var welcomeTip = localStorage.getItem("welcome-tip");
												if(welcomeTip != 'hidden'){
													$('.welcome-tip').addClass('active');
												} else {
													$('.welcome-tip').removeClass('active');
												}
												$('#welcome-tip-close').on('click',function(){
													localStorage.setItem("welcome-tip", "hidden");
													$('.welcome-tip').removeClass('active');
												});

												$(window).on('scroll',function(){
													if($(window).scrollTop() >  200){
														localStorage.setItem("welcome-tip", "hidden");
														$('.welcome-tip').removeClass('active');
													}
												});


											});
										</script>

										</html>